# OpenML dataset: Country_data

https://www.openml.org/d/43739

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
This dataset was a part of the assignment of my coursework.
Content
The dataset contains 90+ columns describing different aspects of all countries like GDP, Population, Electricity-consumption and many more. Most of the fields are explained here (others are standard terms you can search for): link
Acknowledgements
This dataset is taken from CIA
Inspiration
GDP Prediction is the most important task here. Other tasks include the prediction of other fields. Since the dataset is small I want to see how much accuracy can be reached with this.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43739) of an [OpenML dataset](https://www.openml.org/d/43739). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43739/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43739/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43739/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

